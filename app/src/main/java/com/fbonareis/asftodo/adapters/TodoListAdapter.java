package com.fbonareis.asftodo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fbonareis.asftodo.R;
import com.fbonareis.asftodo.models.Todo;

import java.util.List;

public class TodoListAdapter extends ArrayAdapter<Todo> {

    private int resourceLayout;
    private Context mContext;

    public TodoListAdapter(Context context, int resource, List<Todo> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
    {
        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        Todo _todo = getItem(position);

        if (_todo != null) {
            TextView _textDescription = v.findViewById(R.id.text_description);
            TextView _textInclusion = v.findViewById(R.id.text_inclusion);
            ImageView _imgTodoStatus = v.findViewById(R.id.img_todo_status);

            if (_textDescription != null)
                _textDescription.setText(_todo.getDescription());

            if (_textInclusion != null) {
                String _date = DateFormat.format("dd/MM/yyyy - HH:mm", _todo.getInclusion()).toString();
                _textInclusion.setText(_date + "h");
            }

            if(_todo.IsDone() == 1)
            {
                _imgTodoStatus.setImageResource(R.mipmap.ic_todo_status_done);
                _imgTodoStatus.setAlpha(1f);
            }

            else {
                _imgTodoStatus.setImageResource(R.mipmap.ic_todo_status);
                _imgTodoStatus.setAlpha(0.7f);
            }
        }

        return v;
    }
}
