package com.fbonareis.asftodo.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fbonareis.asftodo.models.Todo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class TodoRepository {

    private static SQLiteDatabase _db;
    private static DBUtils _banco;

    public static ArrayList<Todo> TodoItems;

    public TodoRepository(Context context) {
        _banco = new DBUtils(context);
        TodoItems = new ArrayList<>();
    }

    public boolean AddItem(String todo) {
        ContentValues _values;
        long _result;

        _db = _banco.getWritableDatabase();

        _values = new ContentValues();
        _values.put(DBUtils.DESCRIPTION, todo);
        _values.put(DBUtils.ISDONE, 0);
        _values.put(DBUtils.INCLUSION, getDateTime());

        _result = _db.insert(DBUtils.TABLE, null, _values);

        _db.close();

        return _result != -1;
    }

    public boolean UpdateItem(int id, int isDone)
    {
        ContentValues _values;
        long _result;

        _db = _banco.getWritableDatabase();

        _values = new ContentValues();
        _values.put(DBUtils.ISDONE, isDone);

        _result = _db.update(DBUtils.TABLE, _values, "_id=" + id, null);

        return _result != -1;
    }

    public boolean RemoveItem(int id)
    {
        long _result;
        _db = _banco.getWritableDatabase();

        _result = _db.delete(DBUtils.TABLE, "_id=" + id, null);

        return  _result != -1;
    }

    public ArrayList<Todo> getTodoItems(int isDone) {
        Cursor mCursor = loadTodos(isDone);

        ArrayList<Todo> _listTodo = new ArrayList<Todo>();

        while (!mCursor.isAfterLast()) {

            _listTodo.add(new Todo(
                    mCursor.getInt(mCursor.getColumnIndex("_id")),
                    mCursor.getString(mCursor.getColumnIndex("description")),
                    getDate(mCursor.getString(mCursor.getColumnIndex("inclusion"))),
                    mCursor.getInt(mCursor.getColumnIndex("isdone"))
            ));

            mCursor.moveToNext();
        }

        mCursor.close();

        return _listTodo;
    }

    private Cursor loadTodos(int isDone)
    {
        _db = _banco.getWritableDatabase();
        Cursor cursor;

        if(isDone == 2)
        {
            cursor = _db.rawQuery("SELECT _id, description, inclusion, isdone FROM todo", null);
        }

        else
        {
            cursor = _db.rawQuery("SELECT _id, description, inclusion, isdone FROM todo WHERE isdone = ?",
                new String[]{Integer.toString(isDone)});
        }

        if (cursor != null)
            cursor.moveToFirst();

        _db.close();

        return cursor;
    }


    public Date getDate(String dateString)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedDate;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}