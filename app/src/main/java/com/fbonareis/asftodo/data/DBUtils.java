package com.fbonareis.asftodo.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBUtils extends SQLiteOpenHelper {

    private static final String DB_NAME = "asf-database.db";
    public static final String TABLE = "todo";
    public static final String ID = "_id";
    public static final String DESCRIPTION = "description";
    public static final String INCLUSION = "inclusion";
    public static final String ISDONE = "isdone";
    private static final int VERSION = 18;

    public DBUtils(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + TABLE + "("
                + ID + " integer primary key autoincrement,"
                + DESCRIPTION + " text,"
                + INCLUSION + " datetime,"
                + ISDONE + " integer"
                +")";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(db);
    }
}
