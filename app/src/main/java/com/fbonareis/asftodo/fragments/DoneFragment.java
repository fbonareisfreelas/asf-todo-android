package com.fbonareis.asftodo.fragments;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.fbonareis.asftodo.MainActivity;
import com.fbonareis.asftodo.R;
import com.fbonareis.asftodo.adapters.TodoListAdapter;
import com.fbonareis.asftodo.data.TodoRepository;
import com.fbonareis.asftodo.models.Todo;

/**
 * A simple {@link Fragment} subclass.
 */
public class DoneFragment extends Fragment {

    private static TodoRepository _todoRepository;

    public static TodoListAdapter _adapter;
    public static TextView _empty;


    public DoneFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View _view = inflater.inflate(R.layout.fragment_todo, container, false);
        final ListView _list =  _view.findViewById(R.id.todo_list);

        _empty =  _view.findViewById(R.id.empty);

        _todoRepository = new TodoRepository(_view.getContext());

        _adapter = new TodoListAdapter(_view.getContext(), R.layout.adapter_todo_list, _todoRepository.getTodoItems(1));

        _list.setAdapter(_adapter);

        ((MainActivity) getActivity()).setActionBarTitle("Feitas (" + _adapter.getCount() + ")");


        _empty.setText("Você possui nenhuma tarefa feita.\nQue tal finalizar algumas?");
        _empty.setAlpha(_adapter.getCount() > 0 ? 0 : 1);

        _list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(_view.getContext());
                builder.setTitle("O que deseja fazer com essa tarefa?");

                builder.setPositiveButton("Marcar como não feita", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int _id = ((Todo)_list.getItemAtPosition(position)).getId();
                        String _description = ((Todo)_list.getItemAtPosition(position)).getDescription();

                        Boolean _result = _todoRepository.UpdateItem(_id, 0);

                        if(_result) {

                            UpdateList();
                            ((MainActivity) getActivity()).setActionBarTitle("Feitas (" + _adapter.getCount() + ")");

                            Toast.makeText(builder.getContext(), "A tarefa '" + _description + "' foi marcada como não feita.", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });

                builder.setNegativeButton("Remover", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        int _id = ((Todo)_list.getItemAtPosition(position)).getId();
                        String _description = ((Todo)_list.getItemAtPosition(position)).getDescription();

                        Boolean _result = _todoRepository.RemoveItem(_id);

                        if(_result)
                        {
                            UpdateList();

                            Toast.makeText(builder.getContext(), "A tarefa '" + _description + "' foi removida!", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                });

                builder.setNeutralButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }
        });

        return _view;

    }

    public static void UpdateList()
    {
        _adapter.clear();
        _adapter.addAll(_todoRepository.getTodoItems(1));
        _adapter.notifyDataSetChanged();

        _empty.setAlpha(_adapter.getCount() > 0 ? 0 : 1);
    }
}
