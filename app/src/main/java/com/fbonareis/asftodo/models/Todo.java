package com.fbonareis.asftodo.models;

import java.util.Date;

public class Todo
{
    private Integer Id;
    private String Description;
    private Date Inclusion;
    private Integer IsDone;

    public Todo(Integer id, String description, Date inclusion, int isDone)
    {
        Id = id;
        Description = description;
        Inclusion = inclusion;
        IsDone = isDone;
    }

    public Integer getId()
    {
        return this.Id;
    }

    public String getDescription()
    {
        return this.Description;
    }

    public Date getInclusion()
    {
        return this.Inclusion;
    }

    public Integer IsDone()
    {
        return this.IsDone;
    }

    @Override
    public String toString() {
        return Description.toString();
    }
}
