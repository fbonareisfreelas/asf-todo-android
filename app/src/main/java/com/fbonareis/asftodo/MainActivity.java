package com.fbonareis.asftodo;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.fbonareis.asftodo.data.TodoRepository;
import com.fbonareis.asftodo.fragments.AllFragment;
import com.fbonareis.asftodo.fragments.DoneFragment;
import com.fbonareis.asftodo.fragments.TodoFragment;

public class MainActivity extends AppCompatActivity {

    private String m_Text = "";

    public TodoRepository _todoRepository;

    public MainActivity()
    {
        _todoRepository = new TodoRepository(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        setTitle(R.string.title_todo);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        LoadFragment(new TodoFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.action_new:

                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Nova Tarefa");
                builder.setMessage("Qual vai ser o íncrivel nome da sua tarefa?");

                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
                FrameLayout container = new FrameLayout(this);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                );
                params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
                params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
                input.setLayoutParams(params);
                container.addView(input);

                builder.setView(container);

                builder.setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_Text = input.getText().toString();

                        if(!TextUtils.isEmpty(input.getText()))
                        {
                            if(_todoRepository.AddItem(m_Text))
                            {
                                Toast.makeText(builder.getContext(), "Tarefa adicionada com sucesso!",
                                        Toast.LENGTH_SHORT).show();

                                TodoFragment.UpdateList();
                                AllFragment.UpdateList();
                            }

                            else {
                                Toast.makeText(builder.getContext(), "Ops, não foi possivel adicionar essa tarefa!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

                break;
            default:
                break;
        }

        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_todo:
                    setTitle(R.string.title_todo);
                    LoadFragment(new TodoFragment());
                    return true;

                case R.id.navigation_all:
                    setTitle(R.string.title_all);
                    LoadFragment(new AllFragment());
                    return true;

                case R.id.navigation_done:
                    setTitle(R.string.title_done);
                    LoadFragment(new DoneFragment());
                    return true;
            }
            return false;
        }
    };

    private void LoadFragment(Fragment fragment)
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
